package devops.katas.adminprovider;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

    @GetMapping("/admin/{id}")
    public String getAdmin(@PathVariable String id) {
        return String.format("I am admin, id is %s", id);
    }
}
